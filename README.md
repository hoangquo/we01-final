# Le Web est-il devenu trop compliqué ? 

Depuis sa genèse dans les années 1990, les technologies du Web se sont progressivement complexifiées pour répondre aux nouveaux besoins des utilisateurs ainsi que pour corriger les défauts dans sa conception initiale. 

## Une complexité à plusieurs couches
Les complexités de la couche applicative avec laquelle la majorité des utilisateurs interface sont les plus visibles; même en l'absence des compétences techniques, les internautes peuvent distinguer des sites statiques et des sites dits "interactives". L'amélioration de ces techniques affectent donc bien les utilisateurs non techniques. 

Néanmoins, les observations de Stéphane Bortzmeyer semblent se restreindre uniquement à la couche applicative. Il est alors important de noter que sans qu'un utilisateur lambda en soit conscient, l'incorporation des mécanismes de sécurité dans les protocoles comme HTTPS ou l'amélioration de la capacité des réseaux le concerne autant. Tout de même, au contraire de la couche applicative, la conception et le développement des couches basses s'inscrivent plutôt dans le courant d'une culture libre. L'élaboration des normes par l'IETF (dont notamment TCP/IP) se passe dans le cadre d'un processus ouvert auquelle toute personne intéressée peut participer et avoir tous les documents librement. 

Quant à La standardisation et la définition des couches applicatives (HTML5, HTML, XHTML, XML, CSS...), elle, est encadrée par le consortium W3C qui fonctionne de manière plus centralisée et qui est piloté par des représentants des grandes entreprises comme Microsoft, Samsung, Intel ou Google. Un contraste intéressant se voit alors entre la centralisation de la couche applicative à l'opposé des couches basses (mis à part de la détention des protocoles de routage de Cisco).

## La centralisation des moteurs de recherche
Les interprétateurs de Javascript, le moteur de rendu, les questions de comptabilités sur plusieurs architectures et systèmes d'exploitation différents transforment le développement d'un navigateur en une tâche complexe qui nécessite beaucoup trop d'investissement pour un individu ou des petites organisations. Cette complexité semble même dépasser les géants du web comme Google qui a publié une branche open-source de son navigateur Chrome initulée Chromium pour partager la charge de correction des bug à la communauté. 

Néanmoins, parmi ces navigateurs, nous retrouvons en contre balance Mozilla Firefox, un navigateur libre et développé en open-source. Il est tout de même important de noter que son modèle de rentabilité se fonde sur la vente de la position de moteur de recherche par défaut dans son navigateur.

Au niveau des sites, il existe aussi un ensemble de trackeurs et une collecte des méta-données des navigateurs qui permettent à l'industrie publicitaire à nous cibler. Même si certains plug-ins (comme ublock) permettent de se protéger contre ces traçages, ces modules d'extensions dépendent fortement du navigateur et des conditions d'utilisations qu'il impose (rien n'empêche à Chrome ou Firefox d'interdire ces extensions sur son navigateur). 

## Le Projet Gemini
De ce fait, Stéphane Bortzmeyer souhaite dans le projet Gemini construire un protocole et ensuite un navigateur délibérement simple (sans JS ni CSS), non extensible qui permet d'éviter de transmettre des informations pouvant servir au pistage. Néanmoins, il est important de noter qu'un protocole est destiné d'être parlé entre différentes machines. Le premier problème avec cette proposition vient alors du problème d'adoption de l'outil; comme pour Dillo, si une communauté ne s'aggrège pas autour de l'outil et en supposant que le projet Gemini soit open-source, le projet va rapidement devenir obsolète. A cet égard, le navigateur doit permettre de naviguer les sites "classiques" en restreignant le transfert du javascript et des fiches de style. 

Les contenus peuvent alors maintenir la structure du HTML classique afin de garantir la compatibilité pour le navigateur tout en facilitant le développement des nouveaux contenus. La modification majeure devra donc porter sur le parser du contenu, qui face à des balises de style, du JS ou contenant des publicités doit ne pas générer un rendu. 

Il serait donc intéressant pour le projet Gemini de se fonder sur un navigateur minimaliste sur terminal (du style lynx) puis de construire ce parser particulier des contenus HTML pour construire un PoC.




---
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce document est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.

Elle permet:
- le partage du contenu et de ses adaptation sous tous formats
- son utilisation commerciale
Il faudrait bien sûr créditer le document et intégrer un lien vers la license.


